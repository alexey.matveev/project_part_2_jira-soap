package com.cgi.jira.persistent.repository;


import com.cgi.jira.persistent.entities.Ticket;
import com.cgi.jira.persistent.mappers.TicketRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TicketRepository {


    JdbcTemplate jdbcTemplate;

    @Autowired
    public TicketRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //CREATE TICKET
    public int createTicket(Ticket ticket) {
        String sql = "INSERT INTO ticket_jira(\n" +
                "name, id_person_creator, id_person_assigned, creation_datetime, ticket_close_datetime) \n" +
                "VALUES (?, ?, ?, ?, ?)";
        return jdbcTemplate.update(sql, ticket.getName(),
                ticket.getIdPersonCreator(), ticket.getIdPersonAssigned(),
                ticket.getCreationDatetime(), ticket.getTicketCloseDatetime());
    }


    // GET ALL TICKETS
    public List<Ticket> findAllTickets() {
        String sql = "SELECT * FROM ticket_jira";

        return jdbcTemplate.query(sql, new TicketRowMapper());
    }

    //GET TICKET BY ID
    public Ticket findTicketById(long idTicket) {

        System.out.println("Try fo sql 1");
        String sql = "SELECT * FROM ticket_jira WHERE ticket_jira.id_ticket = " + idTicket;

        System.out.println("Try fo sql 2");
        Ticket ticket = jdbcTemplate.queryForObject(sql, new TicketRowMapper());
        System.out.println("Ticket from repo" + ticket);
        return ticket;
    }

    //GET TICKET BY NAME
    public Ticket findTicketByName(String name) {
        String sql = "SELECT * FROM ticket_jira WHERE ticket_jira.name = \'" + name + '\'';
        return jdbcTemplate.queryForObject(sql, new TicketRowMapper());
    }



}
