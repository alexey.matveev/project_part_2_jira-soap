package com.cgi.jira.persistent.mappers;


import com.cgi.jira.persistent.entities.Ticket;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TicketRowMapper implements RowMapper<Ticket> {

    @Override
    public Ticket mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Ticket(
//                resultSet.getLong(1),
//                resultSet.getString(2),
//                resultSet.getLong(3),
//                resultSet.getLong(4),
//                resultSet.getTimestamp(5).toLocalDateTime(),
//                resultSet.getTimestamp(6).toLocalDateTime()

                resultSet.getLong("id_ticket"),
                resultSet.getString("name"),
                resultSet.getLong("id_person_creator"),
                resultSet.getLong("id_person_assigned"),
                resultSet.getTimestamp("creation_datetime").toLocalDateTime(),
                resultSet.getTimestamp("ticket_close_datetime").toLocalDateTime()
                );
    }
}
