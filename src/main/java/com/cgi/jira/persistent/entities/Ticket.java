package com.cgi.jira.persistent.entities;

import java.time.LocalDateTime;

public class Ticket {

    protected long idTicket;
    protected String name;
 //   protected String email;      DELETE
    protected long idPersonCreator;
    protected long idPersonAssigned;
    protected LocalDateTime creationDatetime;
    protected LocalDateTime ticketCloseDatetime;

    public Ticket() {
    }

    public Ticket(long idTicket, String name, long idPersonCreator, long idPersonAssigned, LocalDateTime creationDatetime, LocalDateTime ticketCloseDatetime) {
        this.idTicket = idTicket;
        this.name = name;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDatetime = creationDatetime;
        this.ticketCloseDatetime = ticketCloseDatetime;
    }

    public long getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(long idTicket) {
        this.idTicket = idTicket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public LocalDateTime getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(LocalDateTime creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    public LocalDateTime getTicketCloseDatetime() {
        return ticketCloseDatetime;
    }

    public void setTicketCloseDatetime(LocalDateTime ticketCloseDatetime) {
        this.ticketCloseDatetime = ticketCloseDatetime;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "idTicket=" + idTicket +
                ", name='" + name + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", creationDatetime=" + creationDatetime +
                ", ticketCloseDatetime=" + ticketCloseDatetime +
                '}';
    }
}
