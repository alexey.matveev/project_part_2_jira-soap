package com.cgi.jira.soap.endpoints;


import com.cgi.jira.persistent.entities.Ticket;
import com.cgi.jira.service.TicketService;

import com.ticket_jira_soap.ws.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Endpoint
public class TicketEndPoint {

    private static final String NAMESPACE_URI = "http://ticket-jira-soap.com/ws";
    //    private static final String NAMESPACE_URI = "http://localhost:8080/ws";
    private TicketService ticketService;

    @Autowired
    public TicketEndPoint(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllTicketsRequest")
    @ResponsePayload
    public GetAllTicketsResponse getAllTickets(@RequestPayload GetAllTicketsRequest request) {
        GetAllTicketsResponse getAllTicketsResponse = new GetAllTicketsResponse();
        List<TicketSoap> ticketSoapList = new ArrayList<>();
        List<Ticket> ticketList = ticketService.findAllTickets();
        for (Ticket ticket : ticketList) {
            TicketSoap ticketSoap = new TicketSoap();
            BeanUtils.copyProperties(ticket, ticketSoap);
            try {
                ticketSoap.setCreationDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticket.getCreationDatetime().format(DateTimeFormatter.ISO_DATE_TIME)));
                ticketSoap.setTicketCloseDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticket.getTicketCloseDatetime().format(DateTimeFormatter.ISO_DATE_TIME)));
            } catch (DatatypeConfigurationException e) {
                e.printStackTrace();
            }
            ticketSoapList.add(ticketSoap);
        }
        getAllTicketsResponse.getTicketList().addAll(ticketSoapList);
        return getAllTicketsResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetTicketByIdRequest")
    @ResponsePayload
    public GetTicketByIdResponse getTicketByIdResponse(@RequestPayload GetTicketByIdRequest request) {
        GetTicketByIdResponse getTicketByIdResponse = new GetTicketByIdResponse();
        Ticket ticket = ticketService.findTicketById(request.getId());
        TicketSoap ticketSoap = new TicketSoap();
        BeanUtils.copyProperties(ticket, ticketSoap);
        try {
            ticketSoap.setCreationDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticket.getCreationDatetime().format(DateTimeFormatter.ISO_DATE_TIME)));
            ticketSoap.setTicketCloseDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticket.getTicketCloseDatetime().format(DateTimeFormatter.ISO_DATE_TIME)));
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        getTicketByIdResponse.setTicket(ticketSoap);
        return getTicketByIdResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetTicketByNameRequest")
    @ResponsePayload
    public GetTicketByNameResponse getTicketByNameResponse(@RequestPayload GetTicketByNameRequest request) {
        GetTicketByNameResponse getTicketByNameResponse = new GetTicketByNameResponse();
        Ticket ticket = ticketService.findTicketByName(request.getName());
        TicketSoap ticketSoap = new TicketSoap();
        BeanUtils.copyProperties(ticket, ticketSoap);
        try {
            ticketSoap.setCreationDatetime(DatatypeFactory.newInstance()
                    .newXMLGregorianCalendar(ticket.getCreationDatetime()
                            .format(DateTimeFormatter.ISO_DATE_TIME)));
            ticketSoap.setTicketCloseDatetime(DatatypeFactory.newInstance()
                    .newXMLGregorianCalendar(ticket.getTicketCloseDatetime()
                            .format(DateTimeFormatter.ISO_DATE_TIME)));
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        getTicketByNameResponse.setTicket(ticketSoap);
        return getTicketByNameResponse;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "AddTicketRequest")
    @ResponsePayload
    public AddTicketResponse getTicketByNameResponse(@RequestPayload AddTicketRequest request) {
        AddTicketResponse addTicketResponse = new AddTicketResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        Ticket ticket = new Ticket();
        TicketSoap ticketSoap = request.getTicket();

        BeanUtils.copyProperties(ticketSoap, ticket);

        ticket.setCreationDatetime(ticketSoap.getCreationDatetime().toGregorianCalendar().toZonedDateTime().toLocalDateTime());
        ticket.setTicketCloseDatetime(ticketSoap.getTicketCloseDatetime().toGregorianCalendar().toZonedDateTime().toLocalDateTime());

        //Print to console
        System.out.println("idcreator: " + ticketSoap.getIdPersonCreator() + "  idassigned: " + ticketSoap.getIdPersonAssigned());
        System.out.println(ticket);

        serviceStatus.setStatusCode(ticketService.createTicket(ticket));

        String msg;
        msg = serviceStatus.getStatusCode() == 1 ? "SUCCESS" : "FAILED";
        serviceStatus.setMessage(msg);
        addTicketResponse.setServiceStatus(serviceStatus);

        return addTicketResponse;
    }

}


