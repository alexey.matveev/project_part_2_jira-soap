package com.cgi.jira.service;


import com.cgi.jira.persistent.entities.Ticket;
import com.cgi.jira.persistent.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketService {


    private TicketRepository ticketRepository;

    @Autowired
    public TicketService(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    //CREATE TICKET
    public int createTicket(Ticket ticket) {
        return ticketRepository.createTicket(ticket);
    }

    // GET ALL TICKETS
    public List<Ticket> findAllTickets() {

        return ticketRepository.findAllTickets();
    }

    //GET TICKET BY ID
    public Ticket findTicketById(long idTicket) {

        return ticketRepository.findTicketById(idTicket);
    }

    //GET TICKET BY NAME
    public Ticket findTicketByName(String name) {

        return ticketRepository.findTicketByName(name);
    }



}
